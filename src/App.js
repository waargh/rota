import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import particlesJS from 'particles.js';

import AppBar from 'material-ui/AppBar';

particlesJS.load('particles-js', 'assets/particles.json', function() { console.log('callback - particles.js config loaded'); });

class App extends Component {
  
  render() {
    
    return (
      
      <div className="App">
        <AppBar />
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
   
        <p className="App-intro">
          
          
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
